from django.http import JsonResponse

from .models import Conference, Location


def api_list_conferences(request):
    response = []
    conferences = Conference.objects.all()
    for conference in conferences:
        response.append(
            {
                "name": conference.name,
                "href": conference.get_api_url(),
            }
        )
    return JsonResponse({"conferences": response})


def api_show_conference(request, id):

    conference = Conference.objects.get(id=id)
    return JsonResponse(
        {
            "name": conference.name,
            "starts": conference.starts,
            "ends": conference.ends,
            "description": conference.description,
            "created": conference.created,
            "updated": conference.updated,
            "max_presentations": conference.max_presentations,
            "max_attendees": conference.max_attendees,
            "location": {
                "name": conference.location.name,
                "href": conference.location.get_api_url(),
            },
        }
    )

def api_list_locations(request):
    list_location = []
    locations = Location.objects.all()
    for location in locations:
        list_location.append(
    {       "name": location.name,
            "href": location.get_api_url(),
            }
        )

    return JsonResponse({"locations" : list_location})


def api_show_location(request, id):

    locations = Location.objects.get(id=id)


    return JsonResponse({
        "name": locations.name,
        "city": locations.city,
        "room_count": locations.room_count,
        "created": locations.created,
        "updated": locations.updated,
        "state": locations.state.abbreviation,
    })
